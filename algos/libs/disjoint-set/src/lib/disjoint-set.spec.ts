import { DisjointSet } from './disjoint-set';

const ALPHAS = 'abcdefghijklmnopqrstuvwxyz';
const VOWELS = 'aeiou';
const CONSONANTS = 'bcdfghjklmnpqrstvwxyz';

describe('DisjointSet', () => {
  it('should accept an iterable in constructor', () => {
    const disjointSet = new DisjointSet(ALPHAS);
    for (const a of ALPHAS) {
      for (const b of ALPHAS) {
        expect(disjointSet.areDisjoint(a, b)).toEqual(a != b);
      }
    }
  });

  it('should allow to merge partitions', () => {
    const disjointSet = new DisjointSet(ALPHAS);

    disjointSet.merge('e', 'i');
    disjointSet.merge('e', 'o');
    disjointSet.merge('a', 'u');
    disjointSet.merge('a', 'e');

    for (const c of CONSONANTS) {
      disjointSet.merge('b', c);
    }

    cartesianProduct(VOWELS, VOWELS, (v1, v2) =>
      expect(disjointSet.areJoint(v1, v2)).toBeTruthy()
    );

    cartesianProduct(CONSONANTS, CONSONANTS, (c1, c2) =>
      expect(disjointSet.areJoint(c1, c2)).toBeTruthy()
    );

    cartesianProduct(VOWELS, CONSONANTS, (v, c) =>
      expect(disjointSet.areDisjoint(v, c)).toBeTruthy()
    );
  });
});

function cartesianProduct<A, B>(
  as: Iterable<A>,
  bs: Iterable<B>,
  f: (a: A, b: B) => void
) {
  for (const a of as) {
    for (const b of bs) {
      f(a, b);
    }
  }
}
