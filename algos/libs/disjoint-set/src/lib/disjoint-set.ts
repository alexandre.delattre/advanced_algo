export class DisjointSet<T> {
  private readonly parentsMap = new Map<T, Info<T>>();

  constructor(elems: Iterable<T>) {
    for (const elem of elems) {
      this.parentsMap.set(elem, new Info(elem));
    }
  }

  add(elem: T): boolean {
    if (this.parentsMap.has(elem)) {
      return false;
    }
    this.parentsMap.set(elem, new Info(elem));
    return true;
  }

  findPartition(elem: T): T {
    const info = this.parentsMap.get(elem);
    if (info == null) {
      throw Error(`${elem} not in disjoint set`);
    }
    if (info.root == elem) {
      return elem;
    }
    const newRoot = this.findPartition(info.root);
    info.root = newRoot;
    return newRoot;
  }

  merge(elem1: T, elem2: T): boolean {
    const r1 = this.findPartition(elem1);
    const r2 = this.findPartition(elem2);
    if (r1 == r2) {
      return false;
    }
    /* eslint-disable @typescript-eslint/no-non-null-assertion */
    const info1 = this.parentsMap.get(r1)!;
    const info2 = this.parentsMap.get(r2)!;
    /* eslint-enable @typescript-eslint/no-non-null-assertion */
    if (info1.rank >= info2.rank) {
      info2.root = info1.root;
      info1.rank += info2.rank;
    } else {
      info1.root = info2.root;
      info2.rank += info1.rank;
    }
    return true;
  }

  areDisjoint(elem1: T, elem2: T) {
    const r1 = this.findPartition(elem1);
    const r2 = this.findPartition(elem2);
    return r1 != r2;
  }

  areJoint(elem1: T, elem2: T) {
    const r1 = this.findPartition(elem1);
    const r2 = this.findPartition(elem2);
    return r1 == r2;
  }
}

class Info<T> {
  constructor(public root: T, public rank: number = 1) {}
}
