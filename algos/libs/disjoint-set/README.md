# disjoint-set

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test disjoint-set` to execute the unit tests via [Jest](https://jestjs.io).
