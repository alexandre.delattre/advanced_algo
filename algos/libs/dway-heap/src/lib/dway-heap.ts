import { notStrictEqual, ok } from 'assert';
import { Ord, nativeOrdering, reversedOrdering } from '@algos/ordering';
export type Index = number;

export class DWayHeap<T> {
  private readonly items: T[];

  constructor(
    private readonly D: number,
    items: T[] = [],
    private readonly ordering: Ord<T> = nativeOrdering()
  ) {
    ok(D > 0, 'invalid branch factor');
    this.items = [...items];
    for (let i = this.firstLeafIndex() - 1; i >= 0; i--) {
      this.pushDown(i);
    }
  }

  private getParentIndex(i: Index): Index {
    return Math.trunc((i - 1) / this.D);
  }

  private firstLeafIndex(): Index {
    const n = this.items.length;
    return Math.trunc((n - 2) / this.D + 1);
  }

  private childIndices(i: Index): readonly [Index, Index] {
    const n = this.items.length;
    const lower = Math.min(this.D * i + 1, n);
    const upper = Math.min(lower + this.D, n);
    return [lower, upper] as const;
  }

  private bubbleUp(index: Index) {
    const current = this.items[index];
    while (index > 0) {
      const parentIndex = this.getParentIndex(index);
      const parentValue = this.items[parentIndex];
      if (this.ordering.isLower(parentValue, current)) {
        this.items[index] = parentValue;
        index = parentIndex;
      } else {
        break;
      }
    }
    this.items[index] = current;
  }

  private highestPriorityChild(i: Index): readonly [Index, T] {
    const [lower, upper] = this.childIndices(i);
    notStrictEqual(lower, upper);

    let highIndex = lower;
    let highValue = this.items[lower];

    for (let i = lower + 1; i < upper; i++) {
      const childValue = this.items[i];
      if (this.ordering.isLower(highValue, childValue)) {
        highValue = childValue;
        highIndex = i;
      }
    }

    return [highIndex, highValue] as const;
  }

  private pushDown(index: Index) {
    const current = this.items[index];
    while (index < this.firstLeafIndex()) {
      const [childIndex, childValue] = this.highestPriorityChild(index);
      if (this.ordering.isLower(current, childValue)) {
        this.items[index] = childValue;
        index = childIndex;
      } else {
        break;
      }
    }
    this.items[index] = current;
  }

  push(item: T) {
    this.items.push(item);
    this.bubbleUp(this.items.length - 1);
  }

  peek(): T | undefined {
    return this.items[0];
  }

  pop(): T | undefined {
    const last = this.items.pop();
    if (last == null) {
      return;
    }
    if (this.items.length == 0) {
      return last;
    } else {
      const top = this.items[0];
      this.items[0] = last;
      this.pushDown(0);
      return top;
    }
  }

  remove(predicate: (value: T) => boolean) {
    const index = this.items.findIndex((v) => predicate(v));
    if (index == -1) {
      return;
    }

    const value = this.items[index];
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const last = this.items.pop()!;
    if (index < this.items.length) {
      this.items[index] = last;
      if (this.ordering.isLower(value, last)) {
        this.bubbleUp(index);
      } else {
        this.pushDown(index);
      }
    }
  }

  public get length(): number {
    return this.items.length;
  }

  getItems(): T[] {
    return [...this.items];
  }
}

export function findMaxK<T>(
  items: T[],
  k: number,
  ordering: Ord<T> = nativeOrdering(),
  D = 4
) {
  ok(k > 0, 'k should be positive');
  const heap = new DWayHeap(D, [], reversedOrdering(ordering));
  for (const item of items) {
    if (heap.length == k && ordering.isLower(heap.peek()!, item)) {
      heap.pop();
    }
    if (heap.length < k) {
      heap.push(item);
    }
  }
  return heap.getItems();
}
