import { DWayHeap, findMaxK } from './dway-heap';
import { nativeOrdering, reversedOrdering } from '@algos/ordering';
import * as fc from 'fast-check';

fc.configureGlobal({ numRuns: 1_000 });

describe('DWayHeap', () => {
  test('should heap sort', () => {
    fc.assert(
      fc.property(
        fc.integer({ min: 1, max: 10 }),
        fc.array(fc.integer()),
        (D, numbers) => {
          const heap = new DWayHeap<number>(D);

          for (const n of numbers) {
            heap.push(n);
          }
          expect(heap.length).toEqual(numbers.length);

          const out = [];
          for (let i = 0; i < numbers.length; i++) {
            out.push(heap.pop());
          }
          expect(heap.length).toEqual(0);

          numbers.sort((a, b) => b - a);
          expect(out).toEqual(numbers);
        }
      )
    );
  });

  test('should allow to pass comparator', () => {
    fc.assert(
      fc.property(
        fc.integer({ min: 1, max: 10 }),
        fc.array(fc.integer()),
        (D, numbers) => {
          const heap = new DWayHeap<number>(D, [], reversedOrdering());

          for (const n of numbers) {
            heap.push(n);
          }

          const out = [];
          for (let i = 0; i < numbers.length; i++) {
            out.push(heap.pop());
          }

          numbers.sort((a, b) => a - b);
          expect(out).toEqual(numbers);
        }
      )
    );
  });

  test('should heapify on creation', () => {
    fc.assert(
      fc.property(
        fc.integer({ min: 1, max: 10 }),
        fc.array(fc.integer()),
        (D, numbers) => {
          const heap = new DWayHeap<number>(D, numbers);

          expect(heap.length).toEqual(numbers.length);

          const out = [];
          for (let i = 0; i < numbers.length; i++) {
            out.push(heap.pop());
          }

          expect(heap.length).toEqual(0);
          numbers.sort((a, b) => b - a);
          expect(out).toEqual(numbers);
        }
      )
    );
  });

  test('should allow remove', () => {
    fc.assert(
      fc.property(
        fc.integer({ min: 1, max: 10 }),
        fc.array(fc.integer()),
        (D, numbers) => {
          const heap = new DWayHeap<number>(D, numbers);
          expect(heap.length).toEqual(numbers.length);

          const n = Math.floor(Math.random() * numbers.length);
          const [removed] = numbers.splice(n, 1);
          heap.remove((v) => v == removed);

          const out = [];
          for (let i = 0; i < numbers.length; i++) {
            out.push(heap.pop());
          }

          expect(heap.length).toEqual(0);
          numbers.sort((a, b) => b - a);
          expect(out).toEqual(numbers);
        }
      )
    );
  });
});

test('findMaxK', () => {
  fc.assert(
    fc.property(
      fc.integer({ min: 5, max: 15 }),
      fc.array(fc.integer(), {
        minLength: 50,
        maxLength: 50,
      }),
      fc.integer({ min: 1, max: 10 }),
      (k, arr, D) => {
        const maxK = findMaxK(arr, k, nativeOrdering(), D);
        expect(maxK).toHaveLength(k);

        arr.sort((a, b) => a - b);

        expect(maxK).toMatchObject(expect.arrayContaining(arr.slice(-k)));
      }
    )
  );
});
