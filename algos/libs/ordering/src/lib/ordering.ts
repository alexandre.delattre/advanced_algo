export interface Ord<T> {
  isLower(a: T, b: T): boolean;
  isLowerOrEqual(a: T, b: T): boolean;
  isEqual(a: T, b: T): boolean;
  isGreaterOrEqual(a: T, b: T): boolean;
  isGreater(a: T, b: T): boolean;
}

export function nativeOrdering<T>(): Ord<T> {
  return {
    isLower(a: T, b: T) {
      return a < b;
    },
    isLowerOrEqual(a: T, b: T) {
      return a <= b;
    },
    isEqual(a: T, b: T) {
      return a == b;
    },
    isGreaterOrEqual(a: T, b: T) {
      return a >= b;
    },
    isGreater(a: T, b: T) {
      return a > b;
    },
  };
}

export function reversedOrdering<T>(
  ordering: Ord<T> = nativeOrdering()
): Ord<T> {
  return {
    isLower(a: T, b: T) {
      return ordering.isLower(b, a);
    },
    isLowerOrEqual(a: T, b: T) {
      return ordering.isLowerOrEqual(b, a);
    },
    isEqual(a: T, b: T) {
      return ordering.isEqual(b, a);
    },
    isGreaterOrEqual(a: T, b: T) {
      return ordering.isGreaterOrEqual(b, a);
    },
    isGreater(a: T, b: T) {
      return ordering.isGreater(b, a);
    },
  };
}

export function keyedOrdering<T, K>(
  mapper: (t: T) => K,
  ordering: Ord<K> = nativeOrdering()
): Ord<T> {
  return {
    isLower(a: T, b: T) {
      return ordering.isLower(mapper(a), mapper(b));
    },
    isLowerOrEqual(a: T, b: T) {
      return ordering.isLowerOrEqual(mapper(a), mapper(b));
    },
    isEqual(a: T, b: T) {
      return ordering.isEqual(mapper(a), mapper(b));
    },
    isGreaterOrEqual(a: T, b: T) {
      return ordering.isGreaterOrEqual(mapper(a), mapper(b));
    },
    isGreater(a: T, b: T) {
      return ordering.isGreater(mapper(a), mapper(b));
    },
  };
}
