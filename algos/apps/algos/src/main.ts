import { DWayHeap } from '@algos/dway-heap';

const heap = new DWayHeap<number>(5);

for (let i = 0; i < 10; i++) {
  heap.push(i);
  console.log(heap);
  //      console.log(heap);
}

for (let i = 0; i < 10; i++) {
  console.log(heap.pop());
}
