use std::cmp::min;
use std::fmt::Debug;
use std::ops::Range;

#[derive(Debug)]
pub struct DWayHeap<T, const D: usize>
where
    T: Clone + Ord,
{
    items: Vec<T>,
}

impl<T, const D: usize> Default for DWayHeap<T, D>
where
    T: Clone + Ord,
{
    fn default() -> Self {
        Self {
            items: Default::default(),
        }
    }
}

impl<T, const D: usize> DWayHeap<T, D>
where
    T: Clone + Ord,
{
    fn swap(&mut self, i: usize, j: usize) {
        let temp = self.items[i].clone();
        self.assign(i, self.items[j].clone());
        self.assign(j, temp);
    }

    fn assign(&mut self, idx: usize, val: T) {
        self.items[idx] = val;
    }

    fn _push(&mut self, val: T) {
        self.items.push(val);
    }

    fn get_parent_index(&self, n: usize) -> Option<usize> {
        if n == 0 {
            None
        } else {
            Some((n - 1) / D)
        }
    }

    fn get_child_indices(&self, i: usize) -> Range<usize> {
        let len = self.items.len();
        let lower = min(D * i + 1, len);
        let upper = min(lower + D, len);
        lower..upper
    }

    fn first_leaf_index(&self) -> usize {
        let len = self.items.len();
        if len >= 2 {
            (len - 2) / D + 1
        } else {
            0
        }
    }

    fn highest_priority_child(&self, i: usize) -> Option<(usize, T)> {
        self.get_child_indices(i)
            .max_by_key(|&n| &self.items[n])
            .map(|n| (n, self.items[n].clone()))
    }

    fn bubble_up(&mut self, initial_index: usize) {
        let mut index = initial_index;
        let current = self.items[index].clone();
        while let Some(parent_index) = self.get_parent_index(index) {
            if self.items[parent_index] < current {
                self.assign(index, self.items[parent_index].clone());
                index = parent_index;
            } else {
                break;
            }
        }

        self.assign(index, current);
    }

    fn push_down(&mut self, initial_index: usize) {
        let mut index = initial_index;
        let current = self.items[index].clone();
        while index < self.first_leaf_index() {
            let (child_index, child) = self.highest_priority_child(index).unwrap();
            if child > current {
                self.assign(index, child);
                index = child_index;
            } else {
                break;
            }
        }
        self.assign(index, current)
    }
}

impl<T, const D: usize> DWayHeap<T, D>
where
    T: Clone + Ord,
{
    pub fn new(items: Vec<T>) -> Self {
        let mut heap = Self { items };
        for i in (0..=(heap.first_leaf_index() - 1)).rev() {
            heap.push_down(i);
        }
        heap
    }

    pub fn push(&mut self, item: T) {
        self._push(item);
        self.bubble_up(self.items.len() - 1)
    }

    pub fn pop(&mut self) -> Option<T> {
        if let Some(last) = self.items.pop() {
            if self.items.is_empty() {
                Some(last)
            } else {
                let top = self.items[0].clone();
                self.assign(0, last);
                self.push_down(0);
                Some(top)
            }
        } else {
            None
        }
    }

    pub fn remove(&mut self, item: &T) {
        if let Some(index) = self.items.iter().position(|t| t == item) {
            let last = self.items.pop().unwrap();
            if index < self.items.len() {
                let is_greater = &last > item;
                self.assign(index, last);
                if is_greater {
                    self.bubble_up(index);
                } else {
                    self.push_down(index);
                }
            }
        }
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn is_empty(&self) -> bool {
        self.items.is_empty()
    }
}
