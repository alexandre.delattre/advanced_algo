use std::{
    iter::Sum,
    ops::{Add, Div, Mul, Sub},
};

use float_ord::FloatOrd;

pub trait Float:
    Default
    + Copy
    + PartialEq
    + PartialOrd
    + Add<Output = Self>
    + Sub<Output = Self>
    + Mul<Output = Self>
    + Sum
    + Div<Output = Self>
{
    type OrdFloat: Ord + Copy;

    fn distance<const K: usize>(from: &[Self; K], to: &[Self; K]) -> Self;
    fn to_ord(self) -> Self::OrdFloat;
    fn from_usize(n: usize) -> Self;

    fn infinity() -> Self;
}

impl Float for f64 {
    type OrdFloat = FloatOrd<f64>;

    fn distance<const K: usize>(from: &[Self; K], to: &[Self; K]) -> Self {
        let mut sum = 0f64;
        for i in 0..K {
            let delta = from[i] - to[i];
            sum += delta * delta;
        }
        sum.sqrt()
    }

    fn to_ord(self) -> Self::OrdFloat {
        FloatOrd(self)
    }

    fn from_usize(n: usize) -> Self {
        n as f64
    }

    fn infinity() -> Self {
        f64::INFINITY
    }
}

impl Float for f32 {
    type OrdFloat = FloatOrd<f32>;

    fn distance<const K: usize>(from: &[Self; K], to: &[Self; K]) -> Self {
        let mut sum = 0f32;
        for i in 0..K {
            let delta = from[i] - to[i];
            sum += delta * delta;
        }
        sum.sqrt()
    }

    fn to_ord(self) -> Self::OrdFloat {
        FloatOrd(self)
    }

    fn from_usize(n: usize) -> Self {
        n as f32
    }

    fn infinity() -> Self {
        f32::INFINITY
    }
}
