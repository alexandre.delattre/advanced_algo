use smallvec::SmallVec;

use super::float_trait::Float;

trait HasBoundingSphere<F, const K: usize> {
    fn get_bounding_sphere(&self) -> &Sphere<F, K>;
}

#[derive(Debug)]
pub struct SSNode<F, I, const K: usize, const M: usize> {
    pub bounding_sphere: Sphere<F, K>,
    pub children: SmallVec<[I; M]>,
}

impl<F, I, const K: usize, const M: usize> SSNode<F, I, K, M>
where
    F: Float,
{
    pub fn new_empty() -> Self {
        let bounding_sphere = Sphere::new_empty();
        let children = SmallVec::new();
        Self {
            bounding_sphere,
            children,
        }
    }

    pub fn with_children(new_child1: I, new_child2: I) -> Self {
        let bounding_sphere = Sphere::new_empty();
        let mut children = SmallVec::new();
        children.push(new_child1);
        children.push(new_child2);
        Self {
            bounding_sphere,
            children,
        }
    }
}

impl<F, I, const K: usize, const M: usize> HasBoundingSphere<F, K> for SSNode<F, I, K, M> {
    fn get_bounding_sphere(&self) -> &Sphere<F, K> {
        &self.bounding_sphere
    }
}

#[derive(Debug)]
pub struct SSLeaf<T, F, const K: usize, const M: usize> {
    pub bounding_sphere: Sphere<F, K>,
    pub points: SmallVec<[Point<T, F, K>; M]>,
}

impl<T, F, const K: usize, const M: usize> SSLeaf<T, F, K, M>
where
    F: Float,
{
    pub fn new_empty() -> Self {
        let bounding_sphere = Sphere::new_empty();
        let points = SmallVec::new();
        Self {
            bounding_sphere,
            points,
        }
    }
}

#[derive(Debug)]
pub struct Sphere<F, const K: usize> {
    pub centroid: [F; K],
    pub radius: F,
}

impl<F, const K: usize> Sphere<F, K>
where
    F: Float,
{
    pub fn new(centroid: [F; K], radius: F) -> Self {
        Self { centroid, radius }
    }

    pub fn new_empty() -> Sphere<F, K> {
        let centroid = [F::default(); K];
        let radius = F::default();
        Sphere { centroid, radius }
    }

    pub fn intersects(&self, point: &[F; K]) -> bool {
        self.distance(point) <= F::default()
    }

    pub fn intersects_sphere(&self, other: &Sphere<F, K>) -> bool {
        F::distance(&self.centroid, &other.centroid) <= self.radius + other.radius
    }

    pub fn distance(&self, point: &[F; K]) -> F {
        F::distance(&self.centroid, point) - self.radius
    }
}

#[derive(Debug, Clone)]
pub struct Point<T, F, const K: usize> {
    pub center: [F; K],
    pub data: T,
}

impl<T, F, const K: usize> Point<T, F, K> {
    pub fn new(center: [F; K], data: T) -> Self {
        Self { center, data }
    }
}

impl<T, F, const K: usize, const M: usize> HasBoundingSphere<F, K> for SSLeaf<T, F, K, M> {
    fn get_bounding_sphere(&self) -> &Sphere<F, K> {
        &self.bounding_sphere
    }
}
