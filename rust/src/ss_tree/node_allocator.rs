use typed_generational_arena::{Arena, IgnoreGeneration, Index};

use super::ss_node::{SSLeaf, SSNode};

pub trait NodeAllocator<T: 'static, F, const K: usize, const M: usize> {
    type Index: Clone;

    fn allocate_node(&mut self, node: SSNode<F, Self::Index, K, M>) -> Self::Index;
    fn allocate_leaf(&mut self, node: SSLeaf<T, F, K, M>) -> Self::Index;

    fn get_node(&self, index: &Self::Index) -> NodeRef<'_, T, F, Self::Index, K, M>;
    fn get_node_mut(&mut self, index: &Self::Index) -> NodeRefMut<'_, T, F, Self::Index, K, M>;

    fn get_ssnode_mut(&mut self, index: &Self::Index) -> &mut SSNode<F, Self::Index, K, M> {
        match self.get_node_mut(index) {
            NodeRefMut::Node(mut_ref) => mut_ref,
            NodeRefMut::Leaf(n) => panic!(),
        }
    }
    fn get_ssleaf_mut(&mut self, index: &Self::Index) -> &mut SSLeaf<T, F, K, M> {
        match self.get_node_mut(index) {
            NodeRefMut::Leaf(mut_ref) => mut_ref,
            NodeRefMut::Node(n) => panic!(),
        }
    }
}

pub enum NodeRef<'a, T, F, I, const K: usize, const M: usize> {
    Node(&'a SSNode<F, I, K, M>),
    Leaf(&'a SSLeaf<T, F, K, M>),
}

pub enum NodeRefMut<'a, T, F, I, const K: usize, const M: usize> {
    Node(&'a mut SSNode<F, I, K, M>),
    Leaf(&'a mut SSLeaf<T, F, K, M>),
}

type NodeArena<N> = Arena<N, usize, IgnoreGeneration>;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub struct RawIndex(usize);

impl RawIndex {
    fn for_node(index: usize) -> Self {
        assert!(index & 1 << 63 == 0);
        RawIndex(index)
    }

    fn for_leaf(index: usize) -> Self {
        RawIndex(index | 1 << 63)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum NodeIndex {
    Node(usize),
    Leaf(usize),
}

impl NodeIndex {
    fn from_raw(raw_index: RawIndex) -> NodeIndex {
        if raw_index.0 & 1 << 63 > 0 {
            NodeIndex::Leaf(raw_index.0 & !(1 << 63))
        } else {
            NodeIndex::Node(raw_index.0)
        }
    }

    fn to_raw(self) -> RawIndex {
        match self {
            NodeIndex::Node(i) => RawIndex(i),
            NodeIndex::Leaf(i) => RawIndex(i | (1 << 63)),
        }
    }
}

#[derive(Debug)]
pub struct BiArenaNodeAllocator<T, F, const K: usize, const M: usize> {
    node_arena: NodeArena<SSNode<F, RawIndex, K, M>>,
    leaf_arena: NodeArena<SSLeaf<T, F, K, M>>,
}

impl<T, F, const K: usize, const M: usize> BiArenaNodeAllocator<T, F, K, M> {
    pub fn new() -> Self {
        let node_arena = NodeArena::new();
        let leaf_arena = NodeArena::new();
        Self {
            node_arena,
            leaf_arena,
        }
    }

    pub fn with_capacity(n: usize) -> Self {
        let node_arena = NodeArena::with_capacity(n);
        let leaf_arena = NodeArena::with_capacity(n);
        Self {
            node_arena,
            leaf_arena,
        }
    }

}

impl<T: 'static, F, const K: usize, const M: usize> NodeAllocator<T, F, K, M>
    for BiArenaNodeAllocator<T, F, K, M>
{
    type Index = RawIndex;

    fn allocate_node(&mut self, node: SSNode<F, Self::Index, K, M>) -> Self::Index {
        RawIndex::for_node(self.node_arena.insert(node).to_idx())
    }

    fn allocate_leaf(&mut self, node: SSLeaf<T, F, K, M>) -> Self::Index {
        RawIndex::for_leaf(self.leaf_arena.insert(node).to_idx())
    }

    fn get_node(&self, index: &Self::Index) -> NodeRef<'_, T, F, Self::Index, K, M> {
        match NodeIndex::from_raw(index.clone()) {
            NodeIndex::Node(n) => NodeRef::Node(self.node_arena.get(Index::from_idx(n)).unwrap()),
            NodeIndex::Leaf(n) => NodeRef::Leaf(self.leaf_arena.get(Index::from_idx(n)).unwrap()),
        }
    }

    fn get_node_mut(&mut self, index: &Self::Index) -> NodeRefMut<'_, T, F, Self::Index, K, M> {
        match NodeIndex::from_raw(index.clone()) {
            NodeIndex::Node(n) => {
                NodeRefMut::Node(self.node_arena.get_mut(Index::from_idx(n)).unwrap())
            }
            NodeIndex::Leaf(n) => {
                NodeRefMut::Leaf(self.leaf_arena.get_mut(Index::from_idx(n)).unwrap())
            }
        }
    }
}
