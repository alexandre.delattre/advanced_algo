use smallvec::SmallVec;

use super::{
    float_trait::Float,
    node_allocator::{BiArenaNodeAllocator, NodeAllocator, NodeRef, NodeRefMut},
    ss_node::{Point, SSLeaf, SSNode, Sphere},
};

#[derive(Debug)]
pub struct SSTree<T, F, A, const K: usize, const MAX: usize, const MIN: usize>
where
    A: NodeAllocator<T, F, K, MAX>,
    T: 'static,
{
    allocator: A,
    root: A::Index,
}

impl<T, F, A, const K: usize, const MAX: usize, const MIN: usize> SSTree<T, F, A, K, MAX, MIN>
where
    F: Float,
    A: NodeAllocator<T, F, K, MAX>,
    T: 'static,
{
    pub fn new(allocator: A, root: A::Index) -> Self {
        if MIN > (MAX - 1) / 2 {
            panic!("Min should be <= (Max - 1) / 2");
        }
        Self { allocator, root }
    }

    fn _search(&self, node_idx: &A::Index, target: &[F; K]) -> Option<&T> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    if &point.center == target {
                        return Some(&point.data);
                    }
                }
                None
            }
            NodeRef::Node(node) => {
                for child in &node.children {
                    if self._get_bounding_sphere(child).intersects(target) {
                        let result = self._search(child, target);
                        if result.is_some() {
                            return result;
                        }
                    }
                }
                None
            }
        }
    }

    fn _get_bounding_sphere(&self, node_idx: &A::Index) -> &Sphere<F, K> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Node(node) => &node.bounding_sphere,
            NodeRef::Leaf(leaf) => &leaf.bounding_sphere,
        }
    }

    fn _get_bounding_sphere_mut(&mut self, node_idx: &A::Index) -> &mut Sphere<F, K> {
        match self.allocator.get_node_mut(node_idx) {
            NodeRefMut::Node(node) => &mut node.bounding_sphere,
            NodeRefMut::Leaf(leaf) => &mut leaf.bounding_sphere,
        }
    }

    fn _insert(&mut self, node_idx: &A::Index, point: Point<T, F, K>) -> Option<A::Index> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Leaf(_) => {
                let leaf = self.allocator.get_ssleaf_mut(node_idx);
                leaf.points.push(point);
                let no_split_needed = leaf.points.len() <= MAX - 1;
                self._update_bounding_envelope(node_idx);
                if no_split_needed {
                    return None;
                }
            }
            NodeRef::Node(n) => {
                let closest_child = self._find_closest_child(n, &point.center);
                if let Some(new_child) = self._insert(&closest_child, point) {
                    let mut_node = self.allocator.get_ssnode_mut(node_idx);
                    mut_node.children.push(new_child);

                    let no_split_needed = mut_node.children.len() <= MAX - 1;
                    self._update_bounding_envelope(node_idx);
                    if no_split_needed {
                        return None;
                    }
                } else {
                    self._update_bounding_envelope(node_idx);
                    return None;
                }
            }
        }

        Some(self._split(node_idx))
    }

    fn _get_centroids(&self, node_idx: &A::Index) -> SmallVec<[([F; K], F); MAX]> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Node(node) => node
                .children
                .iter()
                .map(|n| self._get_bounding_sphere(n))
                .map(|s| (s.centroid, s.radius))
                .collect(),
            NodeRef::Leaf(leaf) => leaf
                .points
                .iter()
                .map(|p| (p.center, F::default()))
                .collect(),
        }
    }

    fn _update_bounding_envelope(&mut self, node_idx: &A::Index) {
        let points = self._get_centroids(node_idx);

        let sphere = self._get_bounding_sphere_mut(node_idx);
        for i in 0..K {
            sphere.centroid[i] =
                points.iter().map(|p| p.0[i]).sum::<F>() / F::from_usize(points.len());
        }

        sphere.radius = points
            .iter()
            .map(|entry| F::distance(&sphere.centroid, &entry.0) + entry.1)
            .max_by_key(|f| f.to_ord())
            .unwrap()
    }

    fn _split(&mut self, node: &A::Index) -> A::Index {
        let split_idx = self._find_split_index(node);
        let new_child = match self.allocator.get_node_mut(node) {
            NodeRefMut::Leaf(leaf) => {
                let points: SmallVec<[Point<T, F, K>; MAX]> =
                    leaf.points.drain(split_idx..).collect();

                let new_child = self.allocator.allocate_leaf(SSLeaf {
                    bounding_sphere: Sphere::new_empty(),
                    points,
                });

                new_child
            }
            NodeRefMut::Node(node_ref) => {
                let children: SmallVec<[A::Index; MAX]> =
                    node_ref.children.drain(split_idx..).collect();

                let new_child = self.allocator.allocate_node(SSNode {
                    bounding_sphere: Sphere::new_empty(),
                    children,
                });

                new_child
            }
        };
        self._update_bounding_envelope(&node);
        self._update_bounding_envelope(&new_child);

        new_child
    }

    fn _find_closest_child(&self, node: &SSNode<F, A::Index, K, MAX>, point: &[F; K]) -> A::Index {
        node.children
            .iter()
            .cloned()
            .map(|n| {
                (
                    F::distance(&self._get_bounding_sphere(&n).centroid, point).to_ord(),
                    n,
                )
            })
            .min_by_key(|c| c.0)
            .unwrap()
            .1
    }

    fn _direction_of_max_variance(&self, node_idx: &A::Index) -> usize {
        let centroids = self._get_centroids(node_idx);

        (0..K)
            .max_by_key(|dir| variance_along_directions(&centroids, *dir).to_ord())
            .unwrap()
    }

    fn _find_split_index(&mut self, node_idx: &A::Index) -> usize {
        let index = self._direction_of_max_variance(node_idx);
        self._sort_entries_by_index(node_idx, index);

        let centroids = self._get_centroids(node_idx);

        assert_eq!(centroids.len(), MAX);

        (MIN..=centroids.len() - MIN)
            .min_by_key(|&i| {
                (variance_along_directions(&centroids[0..=i - 1], index)
                    + variance_along_directions(&centroids[i..], index))
                .to_ord()
            })
            .unwrap()
    }

    fn _sort_entries_by_index(&mut self, node_idx: &A::Index, index: usize) {
        match self.allocator.get_node_mut(node_idx) {
            NodeRefMut::Node(node) => {
                let mut children = node.children.clone();
                children.sort_by_key(|p| self._get_bounding_sphere(p).centroid[index].to_ord());
                self.allocator.get_ssnode_mut(node_idx).children = children;
            }
            NodeRefMut::Leaf(leaf) => {
                leaf.points.sort_by_key(|p| p.center[index].to_ord());
            }
        }
    }

    fn _region_search<'a>(
        &'a self,
        node: &A::Index,
        sphere: &Sphere<F, K>,
        result: &mut Vec<&'a Point<T, F, K>>,
    ) {
        match self.allocator.get_node(node) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    if sphere.intersects(&point.center) {
                        result.push(point);
                    }
                }
            }
            NodeRef::Node(node) => {
                for child in &node.children {
                    if self._get_bounding_sphere(child).intersects_sphere(sphere) {
                        self._region_search(child, sphere, result);
                    }
                }
            }
        }
    }

    fn _nearest_neighbor<'a>(
        &'a self,
        node: &A::Index,
        target: &[F; K],
        mut nn_with_distance: (Option<&'a Point<T, F, K>>, F),
    ) -> (Option<&'a Point<T, F, K>>, F) {
        match self.allocator.get_node(node) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    let distance = F::distance(&point.center, target);
                    if distance < nn_with_distance.1 {
                        nn_with_distance = (Some(&point), distance);
                    }
                }
            }
            NodeRef::Node(node) => {
                let mut children: SmallVec<[_; MAX]> = node
                    .children
                    .iter()
                    .map(|c| (c, self._get_bounding_sphere(c).distance(target)))
                    .collect();

                children.sort_by_key(|c| c.1.to_ord());
                for child in &children {
                    if child.1.to_ord() < nn_with_distance.1.to_ord() {
                        nn_with_distance =
                            self._nearest_neighbor(child.0, target, nn_with_distance);
                    } else {
                        break; //TODO check
                    }
                }
            }
        }
        nn_with_distance
    }
}

fn variance_along_directions<F: Float, const K: usize>(points: &[([F; K], F)], i: usize) -> F {
    let n: F = F::from_usize(points.len());
    let mean = points.iter().map(|p| p.0[i]).sum::<F>() / n;
    let var_2 = points
        .iter()
        .map(|p| p.0[i] - mean)
        .map(|f| f * f)
        .sum::<F>()
        / n;
    var_2
}

impl<T, F, A, const K: usize, const MAX: usize, const MIN: usize> SSTree<T, F, A, K, MAX, MIN>
where
    F: Float,
    A: NodeAllocator<T, F, K, MAX>,
{
    pub fn search(&self, target: &[F; K]) -> Option<&T> {
        self._search(&self.root, target)
    }

    pub fn insert(&mut self, point: Point<T, F, K>) {
        if let Some(new_child) = self._insert(&self.root.clone(), point) {
            let root_idx = self
                .allocator
                .allocate_node(SSNode::with_children(self.root.clone(), new_child));
            self._update_bounding_envelope(&root_idx);
            self.root = root_idx;
        }
    }

    pub fn region_search(&self, sphere: &Sphere<F, K>) -> Vec<&Point<T, F, K>> {
        let mut result = Vec::new();
        self._region_search(&self.root, sphere, &mut result);
        result
    }

    pub fn neareast_neighbor(&self, target: &[F; K]) -> Option<&Point<T, F, K>> {
        self._nearest_neighbor(&self.root, target, (None, F::infinity()))
            .0
    }
}

pub fn new_sstree<T, F: Float, const K: usize, const MAX: usize, const MIN: usize>(
    capacity: usize,
) -> SSTree<T, F, BiArenaNodeAllocator<T, F, K, MAX>, K, MAX, MIN> {
    let mut allocator = BiArenaNodeAllocator::with_capacity(capacity);
    let root = allocator.allocate_leaf(SSLeaf::new_empty());
    SSTree { allocator, root }
}

#[cfg(test)]
mod tests {
    use crate::ss_tree::float_trait::Float;
    use crate::ss_tree::{
        ss_node::{Point, Sphere},
        ss_tree::new_sstree,
    };

    #[test]
    fn test_tree() {
        let mut tree = new_sstree::<i32, f64, 3, 5, 2>(1000);

        let mut i = 0;
        for x in 0..10 {
            for y in 0..10 {
                for z in 0..3 {
                    tree.insert(Point::new(
                        [x as f64 / 100.0, y as f64 / 100.0, z as f64 / 100.0],
                        i,
                    ));
                    i += 1;
                }
            }
        }

        let mut i = 0;
        for x in 0..10 {
            for y in 0..10 {
                for z in 0..3 {
                    let n = *tree
                        .search(&[x as f64 / 100.0, y as f64 / 100.0, z as f64 / 100.0])
                        .unwrap();
                    assert_eq!(n, i);
                    i += 1;
                }
            }
        }

        let sphere = Sphere::new([0.05, 0.05, 0.01], 0.015 + 1e-6);
        let res = tree.region_search(&sphere);

        println!("Searching in {:?}", sphere);

        for point in &res {
            println!(
                "{:?} {}",
                &point,
                f64::distance(&sphere.centroid, &point.center)
            );
        }

        let nn = tree.neareast_neighbor(&[0.021, 0.057, 0.012]);
        println!("{:?}", nn);
        //println!("{:?}", tree);
    }

    #[test]
    fn foo() {
        for i in 0..10 {
            println!("{}", (i as f64) / 100.0);
        }
    }

    // #[test]
    // fn test_index_conversion() {
    //     let node_raw_index = RawIndex::for_node(42);
    //     let leaf_raw_index = RawIndex::for_leaf(42);
    //     assert_eq!(NodeIndex::Node(42), NodeIndex::from_raw(node_raw_index));
    //     assert_eq!(NodeIndex::Leaf(42), NodeIndex::from_raw(leaf_raw_index));

    //     assert_eq!(node_raw_index, NodeIndex::Node(42).to_raw());
    //     assert_eq!(leaf_raw_index, NodeIndex::Leaf(42).to_raw());
    // }
}
